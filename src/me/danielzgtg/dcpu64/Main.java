package me.danielzgtg.dcpu64;

import java.util.LinkedList;
import java.util.List;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import me.danielzgtg.dcpu64.device.PortDevice;
import me.danielzgtg.dcpu64.device.PortDeviceConsolePrintChar;
import me.danielzgtg.dcpu64.device.PortDeviceConsolePrintNum;
import me.danielzgtg.dcpu64.device.PortDeviceGPGPU;
import me.danielzgtg.dcpu64.device.PortDeviceVideoCard;

public class Main {

	public static void main(final String[] ignore) {
		PortDevice[] ports = new PortDevice[4];
		List<Long> tmpMemory = new LinkedList<>();

		try (final InputStream fin = new FileInputStream("program.dcpu64.out")){
			final DataInputStream in = new DataInputStream(fin);

			while (true) {
				try {
					tmpMemory.add(in.readLong());
				} catch (final EOFException e) {
					break;
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		DCPU64 emulator = new DCPU64(
				tmpMemory.stream().mapToLong(x -> x).toArray(), "myemulator", 1, ports);
		tmpMemory = null;
		ports[0] = new PortDeviceConsolePrintNum(emulator);
		ports[1] = new PortDeviceConsolePrintChar(emulator);
		ports[2] = new PortDeviceGPGPU(emulator);
		final PortDeviceVideoCard graphics =
				PortDeviceVideoCard.init(emulator, 640, 480, "Test123", 20, () -> System.exit(0));
		ports[3] = graphics;

		emulator.run();

		tmpMemory = new LinkedList<>();
		int length = emulator.memory.length();
		for (int i = 0; i < length; i++) {
			tmpMemory.add(emulator.memory.get(i));
		}

		try (final OutputStream fout = new FileOutputStream("program.dcpu64.core.out")){
			final DataOutputStream out = new DataOutputStream(fout);

			tmpMemory.forEach(l -> {
				try {
					out.writeLong(l);
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

//		System.exit(0);
		graphics.setTitle("Test123 : Stopped");
	}
}
