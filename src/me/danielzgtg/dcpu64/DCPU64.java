package me.danielzgtg.dcpu64;

import java.util.concurrent.atomic.AtomicLongArray;

import me.danielzgtg.dcpu64.cpu.CpuThread;
import me.danielzgtg.dcpu64.device.PortDevice;

public class DCPU64 implements Runnable {

	public AtomicLongArray memory;
	public final String name;
	public volatile CpuThread[] threads;
	public final Object joinWaitObj = new Object();
	public final PortDevice[] ports;

	public DCPU64(final long[] initialMemory, final String name, final int maxThreads, final PortDevice[] ports) {
		if (maxThreads < 1) throw new IllegalArgumentException();
		if ((initialMemory.length < 0xAAAF) || (initialMemory.length > 0xFFFFFF))
			throw new IllegalArgumentException("Improper length: " + initialMemory.length);

		this.memory = new AtomicLongArray(initialMemory);
		this.name = name;
		this.ports = ports;

		this.threads = new CpuThread[maxThreads];
		this.threads[0] = new CpuThread(0, this, 0xAAAA);
	}

	@Override
	public void run() {
		this.threads[0].start();

		while (true) {
			try {
				synchronized (this.joinWaitObj) {
					this.joinWaitObj.wait();
				}
			} catch (final InterruptedException ignore) {}

			for (final CpuThread thread : threads) {
				if (thread != null) continue;
				return;
			}
		}
	}


}
