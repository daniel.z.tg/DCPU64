package me.danielzgtg.dcpu64.compiler;

import java.util.Map;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class CompilerDataFileOutput extends CompilerData {

	private final OutputStream fout;
	private final OutputStream flout;
	private final String sourceId;

	public CompilerDataFileOutput(final String sourceId, final OutputStream fout, final OutputStream flout) {
		super("");
		this.fout = fout;
		this.flout = flout;
		this.sourceId = sourceId;
	}

	@Override
	public void output() {
		try {
			final DataOutputStream out = new DataOutputStream(fout);
			final Writer lout = new OutputStreamWriter(flout);

			this.finishCompiling(0).forEach(x -> {
				try {
					out.writeLong(x);
				} catch (final IOException e) {
					throw new RuntimeException("IOError while writing: ".concat(sourceId), e);
				}
			});

			this.dumpLabels().entrySet().stream().sorted(Map.Entry.comparingByValue()).forEach(x -> {
				try {
					lout.write(String.format("pos 0x%016X lbl %s\n", x.getValue(), x.getKey()));
				} catch (final IOException e) {
					throw new RuntimeException("IOError while writing: ".concat(sourceId), e);
				}
			});

			out.flush();
			lout.flush();
		} catch (final IOException ioe) {
			throw new IllegalArgumentException(String
					.format("IOError in %s", sourceId));
		}
	}
}
