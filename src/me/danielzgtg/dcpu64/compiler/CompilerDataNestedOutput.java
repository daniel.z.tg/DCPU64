package me.danielzgtg.dcpu64.compiler;

import java.util.Map;

public class CompilerDataNestedOutput extends CompilerData {

	private final CompilerData parent;
	private final String sourceId;

	public CompilerDataNestedOutput(final String sourceId, final CompilerData parent) {
		super(String.format("__included:%s.", sourceId));
		this.parent = parent;
		this.sourceId = sourceId;
	}

	@Override
	public void output() {
		final long offset = this.parent.instructionPos;
		this.parent.instructionList.add(
				new LargeCompilingInstruction(this.finishCompiling(offset).toArray()));

		this.dumpLabels().entrySet().stream().sorted(Map.Entry.comparingByValue()).forEach(x -> {
			boolean absolute = this.isAbsoluteLabel(x.getKey());
			parent.addLabel(
					String.format("__included:%s.%s", sourceId, x.getKey()),
					x.getValue() + (absolute ? 0 : offset), true, absolute
			);
		});

		this.parent.instructionPos += this.instructionPos;
	}
}
