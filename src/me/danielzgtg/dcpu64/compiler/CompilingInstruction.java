package me.danielzgtg.dcpu64.compiler;

public abstract interface CompilingInstruction {

	public abstract long[] compile(final CompilerData state, final long linkOffset);
}
