package me.danielzgtg.dcpu64.compiler;

public final class RelativeLabelJumpTypeInstruction implements CompilingInstruction {

	private final String label;
	private final int lineNum;
	private final int registerMask;
	private final int position;
	public static final int SIZE = 3;
	private final long instruction;

	public RelativeLabelJumpTypeInstruction(final String label, final int lineNum, final int register,
			final int position, final long instruction) {
		this.label = label;
		this.lineNum = lineNum;
		this.registerMask = register << 8;
		this.position = position;
		this.instruction = instruction;
	}

	@Override
	public long[] compile(final CompilerData state, final long linkOffset) {
		final long preTarget = state.resolveLabel(label, lineNum, 0);

		return new long[] {
				registerMask | 0x0001, preTarget - position - 2, registerMask | instruction };
	}
}
