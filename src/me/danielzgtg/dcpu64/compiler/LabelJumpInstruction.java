package me.danielzgtg.dcpu64.compiler;

public final class LabelJumpInstruction implements CompilingInstruction {

	private final String label;
	private final int lineNum;
	private final int registerMask;
	public static final int SIZE = 3;
	private final long instruction;

	public LabelJumpInstruction(final String label, final int lineNum, final int register, final long instruction) {
		this.label = label;
		this.lineNum = lineNum;
		this.registerMask = register << 8;
		this.instruction = instruction;
	}

	@Override
	public long[] compile(final CompilerData state, final long linkOffset) {
		final long target = state.resolveLabel(label, lineNum, linkOffset);

		return new long[] { registerMask | 0x0001, target, registerMask | instruction };
	}
}
