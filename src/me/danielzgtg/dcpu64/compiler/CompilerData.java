package me.danielzgtg.dcpu64.compiler;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.LongStream;

public abstract class CompilerData {

	public final /*mutable*/ List<CompilingInstruction> instructionList = new LinkedList<>();
	private final /*mutable*/ Map<String, Long> labelMap = new HashMap<>();
	private final /*mutable*/ Map<String, Long> hiddenLabelMap = new HashMap<>();
	private final /*mutable*/ Map<String, Boolean> absolutenessMap = new HashMap<>();
	public int lineNum = 0;
	public int instructionPos = 0;
	public final String labelPrefix;

	protected CompilerData(final String labelPrefix) {
		this.labelPrefix = labelPrefix;
	}

	protected LongStream finishCompiling(final long linkOffset) {
		return instructionList.stream().flatMapToLong(x -> Arrays.stream(x.compile(this, linkOffset)));
	}

	public abstract void output();

	public void addLabel(final String name, final long reference, final boolean internal, final boolean absolute) {
		if (!internal && name.startsWith("__includedfile.")) {
			throw new IllegalArgumentException("Cannot use reserved label:  " + name);
		}

		if (this.labelMap.containsKey(name) || this.hiddenLabelMap.containsKey(name)) {
			throw new IllegalArgumentException("Label already in use: " + name);
		}

		(internal ? this.hiddenLabelMap : this.labelMap).put(name, reference);
		this.absolutenessMap.put(name, absolute);
	}

	public Map<String, Long> dumpLabels() {
		final /*mutable*/ Map<String, Long> result = new HashMap<>();

		result.putAll(labelMap);
		result.putAll(hiddenLabelMap);

		return result;
	}

	public long resolveLabel(final String label, final int lineNum, final long linkOffset) {
		final Long result = this.labelMap.get(label);

		if (result == null) {
			throw new IllegalArgumentException(
					String.format("Failed to resolve label: %s at line %d", label, lineNum));
		}

		return result + (this.absolutenessMap.get(label) ? 0 : linkOffset);
	}

	public boolean isAbsoluteLabel(final String label) {
		return Boolean.TRUE.equals(this.absolutenessMap.get(label));
	}
}
