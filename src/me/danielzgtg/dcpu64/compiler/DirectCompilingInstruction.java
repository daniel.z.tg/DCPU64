package me.danielzgtg.dcpu64.compiler;

public final class DirectCompilingInstruction implements CompilingInstruction {
	private final long targetInstruction;
	public static final int SIZE = 1;

	public DirectCompilingInstruction(final long targetInstruction) {
		this.targetInstruction = targetInstruction;
	}

	@Override
	public long[] compile(final CompilerData state, final long linkOffset) {
		return new long[] { targetInstruction };
	}
}
