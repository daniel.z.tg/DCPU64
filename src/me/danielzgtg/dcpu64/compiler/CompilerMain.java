package me.danielzgtg.dcpu64.compiler;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public final class CompilerMain {

	public static final Map<String, BiConsumer<String[], CompilerData>> SUBCOMPILERS;
	public static final Map<String, Integer> REGISTER_CODES;

	static {
		final /*mutable*/ Map<String, Integer> tmpRegisterCodes = new HashMap<>();

		tmpRegisterCodes.put("ax", 0x0);
		tmpRegisterCodes.put("dx", 0x1);
		tmpRegisterCodes.put("sp", 0x2);
		tmpRegisterCodes.put("sbp", 0x3);
		tmpRegisterCodes.put("pc", 0x4);
		tmpRegisterCodes.put("bx", 0x5);
		tmpRegisterCodes.put("cx", 0x6);
		tmpRegisterCodes.put("si", 0x7);
		tmpRegisterCodes.put("di", 0x8);

		REGISTER_CODES = Collections.unmodifiableMap(tmpRegisterCodes);

		final /*mutable*/ Map<String, BiConsumer<String[], CompilerData>> tmpSubcompilers = new HashMap<>();

		tmpSubcompilers.put("include", (final String[] line, final CompilerData state) -> {
			if (line.length != 2) throw new IllegalArgumentException("Included file must be a continuous path!");

			final CompilerData subState = new CompilerDataNestedOutput("file:".concat(line[1]), state);

			doCompileFromPathToState(line[1], subState);
			subState.output();
		});

		tmpSubcompilers.put("blob", (final String[] line, final CompilerData state) -> {
			if (line.length != 2) throw new IllegalArgumentException("Included blob must be a continuous path!");

			try (final InputStream fin = new FileInputStream(line[1].concat(".blob"))) {
				final DataInputStream in = new DataInputStream(fin);
				final /*mutable*/ List<Long> tmpData = new LinkedList<>();

				while (true) {
					try {
						tmpData.add(in.readLong());
					} catch (final EOFException e) {
						break;
					}
				}

				state.instructionList.add(
						new LargeCompilingInstruction(tmpData.stream().mapToLong(x -> x).toArray()));
				state.instructionPos += tmpData.size();
			} catch (final FileNotFoundException fnfe) {
				throw new IllegalArgumentException(
						String.format("File not found: %s", line[1]));
			} catch (final IOException ie) {
				throw new IllegalArgumentException(String
						.format("IOError in %s", line[1]));
			}
		});

		tmpSubcompilers.put("label", (final String[] line, final CompilerData state) -> {
			if (line.length != 2) throw new IllegalArgumentException("Labels must be one word long!");

			state.addLabel(line[1], (long) state.instructionPos, false, false);
		});

		tmpSubcompilers.put("dlabel", (final String[] line, final CompilerData state) -> {
			if (line.length != 3)
				throw new IllegalArgumentException("Declared labels must be one word long and take a 64-bit number");
			final long payload = decodeLong(line[2]);

			state.addLabel(line[1], payload, false, false);
		});

		tmpSubcompilers.put("alabel", (final String[] line, final CompilerData state) -> {
			if (line.length != 3)
				throw new IllegalArgumentException("Absolute labels must be one word long and take a 64-bit number");
			final long payload = decodeLong(line[2]);

			state.addLabel(line[1], payload, false, true);
		});

		tmpSubcompilers.put("pad", (final String[] line, final CompilerData state) -> {
			if (line.length != 2)
				throw new IllegalArgumentException("To pad with NOP, you need to specify a 32-bit length!");
			final int length = decodeInt(line[1]);

			for (int i = 0; i < length; i++) {
				state.instructionList.add(new DirectCompilingInstruction(0x0000));
			}

			state.instructionPos += length;
		});

		tmpSubcompilers.put("dd", (final String[] line, final CompilerData state) -> {
			if (line.length != 2) throw new IllegalArgumentException("DD takes a 64-bit number!");
			final long payload = decodeLong(line[1]);

			state.instructionList.add(new DirectCompilingInstruction(payload));
			state.instructionPos += DirectCompilingInstruction.SIZE;
		});

		tmpSubcompilers.put("dc", (final String[] line, final CompilerData state) -> {
			if (line.length != 2) throw new IllegalArgumentException("DC takes a char!");
			if (line[1].length() != 1) throw new IllegalArgumentException("DC takes a char!");
			final long payload = line[1].charAt(0);

			state.instructionList.add(new DirectCompilingInstruction(payload));
			state.instructionPos += DirectCompilingInstruction.SIZE;
		});

		generateArgumentlessSubcompiler(tmpSubcompilers, "nop", (byte) 0x0000);
		generateRegisterPayloadSubcompiler(tmpSubcompilers, "reg", (byte) 0x0001);

		tmpSubcompilers.put("regl", (final String[] line, final CompilerData state) -> {
			if (line.length != 3) throw new IllegalArgumentException("REGL (REG) takes a register and a 64-bit number!");

			state.instructionList.add(
					new LabelRegisterLoadInstruction(line[2], state.lineNum,
							encodeRegister(line[1])));
			state.instructionPos += LabelRegisterLoadInstruction.SIZE;
		});

		generateTwoRegisterSubcompiler(tmpSubcompilers, "mem", (byte) 0x0002);

		generateTwoRegisterSubcompiler(tmpSubcompilers, "add", (byte) 0x0003);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "sub", (byte) 0x0004);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "mul", (byte) 0x0005);
		generateArgumentlessSubcompiler(tmpSubcompilers, "out", (byte) 0x0006);
		generateArgumentlessSubcompiler(tmpSubcompilers, "in", (byte) 0x0007);

		generateOneRegisterSubcompiler(tmpSubcompilers, "jmp", (byte) 0x0008);
		generateOneRegisterSubcompiler(tmpSubcompilers, "rjmp", (byte) 0x0009);

		tmpSubcompilers.put("jmpl", (final String[] line, final CompilerData state) -> {
			if (line.length != 3) throw new IllegalArgumentException("JMPL (REG + JMP) takes a label and a register!");

			state.instructionList.add(
					new LabelJumpInstruction(line[1], state.lineNum,
							encodeRegister(line[2]), 0x0008));
			state.instructionPos += LabelJumpInstruction.SIZE;
		});

		tmpSubcompilers.put("rjmpl", (final String[] line, final CompilerData state) -> {
			if (line.length != 3) throw new IllegalArgumentException("RJMPL (REG + RJMP) takes a label and a register!");

			state.instructionList.add(
					new RelativeLabelJumpTypeInstruction(line[1], state.lineNum,
							encodeRegister(line[2]), state.instructionPos, 0x0009));
			state.instructionPos += RelativeLabelJumpTypeInstruction.SIZE;
		});

		generateArgumentlessSubcompiler(tmpSubcompilers, "hlt", (byte) 0x000A);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "test", (byte) 0x000B);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "cmp", (byte) 0x000C);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "cpuid", (byte) 0x000D);

		// skip 3 (use with test or cmp)
		generateArgumentlessSubcompiler(tmpSubcompilers, "sz", (byte) 0x000E);
		generateArgumentlessSubcompiler(tmpSubcompilers, "se", (byte) 0x000E);
		generateArgumentlessSubcompiler(tmpSubcompilers, "snz", (byte) 0x000F);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sne", (byte) 0x000F);
		generateArgumentlessSubcompiler(tmpSubcompilers, "ss", (byte) 0x0010);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sns", (byte) 0x0011);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sc", (byte) 0x0012);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sb", (byte) 0x0012);
		generateArgumentlessSubcompiler(tmpSubcompilers, "snc", (byte) 0x0013);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sae", (byte) 0x0013);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sv", (byte) 0x0014);
		generateArgumentlessSubcompiler(tmpSubcompilers, "snv", (byte) 0x0015);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sbe", (byte) 0x0016);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sa", (byte) 0x0017);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sl", (byte) 0x0018);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sge", (byte) 0x0019);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sle", (byte) 0x001A);
		generateArgumentlessSubcompiler(tmpSubcompilers, "sg", (byte) 0x001B);

		// leap 3 (use with one or two registers themselves)
		generateTwoRegisterSubcompiler(tmpSubcompilers, "le", (byte) 0x001C);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "lne", (byte) 0x001D);
		generateOneRegisterSubcompiler(tmpSubcompilers, "lz", (byte) 0x001E);
		generateOneRegisterSubcompiler(tmpSubcompilers, "lnz", (byte) 0x001F);
		generateOneRegisterSubcompiler(tmpSubcompilers, "ls", (byte) 0x0020);
		generateOneRegisterSubcompiler(tmpSubcompilers, "lns", (byte) 0x0021);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "lb", (byte) 0x0022);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "lbe", (byte) 0x0023);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "lg", (byte) 0x0024);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "lge", (byte) 0x0025);

		generateOneRegisterSubcompiler(tmpSubcompilers, "not", (byte) 0x0026);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "and", (byte) 0x0027);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "or", (byte) 0x0028);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "xor", (byte) 0x0029);
		generateOneRegisterSubcompiler(tmpSubcompilers, "inc", (byte) 0x002A);
		generateOneRegisterSubcompiler(tmpSubcompilers, "dec", (byte) 0x002B);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "shl", (byte) 0x002C);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "rol", (byte) 0x002C);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "shr", (byte) 0x002D);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "ror", (byte) 0x002E);

		generateTwoRegisterSubcompiler(tmpSubcompilers, "load", (byte) 0x002F);

		generateOneRegisterSubcompiler(tmpSubcompilers, "push", (byte) 0x0030);
		generateOneRegisterSubcompiler(tmpSubcompilers, "pop", (byte) 0x0031);
		generateArgumentlessSubcompiler(tmpSubcompilers, "enter", (byte) 0x0032);
		generateArgumentlessSubcompiler(tmpSubcompilers, "leave", (byte) 0x0033);
		generateOneRegisterSubcompiler(tmpSubcompilers, "call", (byte) 0x0034);
		generateArgumentlessSubcompiler(tmpSubcompilers, "ret", (byte) 0x0035);
		generateArgumentlessSubcompiler(tmpSubcompilers, "return", (byte) 0x0035);

		generateTwoRegisterSubcompiler(tmpSubcompilers, "mov", (byte) 0x0036);
		generateTwoRegisterSubcompiler(tmpSubcompilers, "xchg", (byte) 0x0037);

		generateOneRegisterSubcompiler(tmpSubcompilers, "rcall", (byte) 0x0038);

		tmpSubcompilers.put("calll", (final String[] line, final CompilerData state) -> {
			if (line.length != 3) throw new IllegalArgumentException("CALLL (REG + CALL) takes a label and a register!");

			state.instructionList.add(
					new LabelJumpInstruction(line[1], state.lineNum,
							encodeRegister(line[2]), 0x0034));
			state.instructionPos += LabelJumpInstruction.SIZE;
		});

		tmpSubcompilers.put("rcalll", (final String[] line, final CompilerData state) -> {
			if (line.length != 3) throw new IllegalArgumentException("RCALLL (REG + RCALL) takes a label and a register!");

			state.instructionList.add(
					new RelativeLabelJumpTypeInstruction(line[1], state.lineNum,
							encodeRegister(line[2]), state.instructionPos, 0x0038));
			state.instructionPos += RelativeLabelJumpTypeInstruction.SIZE;
		});

		generateArgumentlessSubcompiler(tmpSubcompilers, "debug_breakpoint", (byte) 0x0039);

		SUBCOMPILERS = Collections.unmodifiableMap(tmpSubcompilers);
	}

	private static final long decodeLong(final String s) {
		try {
			return s.startsWith("0x") ? Long.parseUnsignedLong(s.substring(2), 16): Long.decode(s);
		} catch (final NumberFormatException|ArithmeticException e) {
			throw new IllegalArgumentException("Malformed 64-bit number! : ".concat(s), e);
		}
	}

	private static final int decodeInt(final String s) {
		try {
			return s.startsWith("0x") ? Integer.parseUnsignedInt(s.substring(2), 16): Integer.decode(s);
		} catch (final NumberFormatException|ArithmeticException e) {
			throw new IllegalArgumentException("Malformed 32-bit number! : ".concat(s), e);
		}
	}

	private static final void generateRegisterPayloadSubcompiler(
			final Map<String, BiConsumer<String[], CompilerData>> map, final String name, final byte instruction) {
		map.put(name, (final String[] line, final CompilerData state) -> {
			if (line.length != 3)
				throw new IllegalArgumentException(name.toUpperCase().concat(" takes one target and a 64-bit number!"));
			final long payload = decodeLong(line[2]);

			state.instructionList.add(new LargeCompilingInstruction(
					new long[] { (encodeRegister(line[1]) << 8) | instruction, payload }));
			state.instructionPos += 2;
		});
	}

	private static final void generateArgumentlessSubcompiler(
			final Map<String, BiConsumer<String[], CompilerData>> map, final String name, final byte instruction) {
		map.put(name, (final String[] line, final CompilerData state) -> {
			if (line.length != 1)
				throw new IllegalArgumentException(name.toUpperCase().concat(" does not take arguments!"));
			state.instructionList.add(new DirectCompilingInstruction(instruction));
			state.instructionPos += DirectCompilingInstruction.SIZE;
		});
	}

	private static final void generateTwoRegisterSubcompiler(
			final Map<String, BiConsumer<String[], CompilerData>> map, final String name, final byte instruction) {
		map.put(name, (final String[] line, final CompilerData state) -> {
			if (line.length != 3) throw new IllegalArgumentException(name.toUpperCase().concat(" takes two targets!"));
			state.instructionList.add(new DirectCompilingInstruction(
					(encodeRegister(line[1]) << 8) | (encodeRegister(line[2]) << 12) | instruction));
			state.instructionPos += DirectCompilingInstruction.SIZE;
		});
	}

	private static final void generateOneRegisterSubcompiler(
			final Map<String, BiConsumer<String[], CompilerData>> map, final String name, final byte instruction) {
		map.put(name, (final String[] line, final CompilerData state) -> {
			if (line.length != 2) throw new IllegalArgumentException(name.toUpperCase().concat(" takes one target!"));
			state.instructionList.add(new DirectCompilingInstruction(
					(encodeRegister(line[1]) << 8) | instruction));
			state.instructionPos += DirectCompilingInstruction.SIZE;
		});
	}

	public static final void compileToState(
			final String sourceId, final InputStream fin, final CompilerData state) {
		final BufferedReader in = new BufferedReader(new InputStreamReader(fin));

		state.lineNum++;
		try {
			/*variable*/ String s = in.readLine();
			while (s != null) {
				final String[] parts = s.replace("\t", " ").replace(" +", " ").trim().split(" ");

				if ((parts.length != 0) && (parts[0].length() != 0) && (!parts[0].startsWith("//"))) {
					final BiConsumer<String[], CompilerData> lineCompiler =
							SUBCOMPILERS.get(parts[0].toLowerCase());

					if (lineCompiler == null) {
						throw new IllegalArgumentException(
								String.format("Don't know what to do at line %d with: %s", state.lineNum, s));
					}

					try {
						lineCompiler.accept(parts, state);
					} catch (final IllegalArgumentException e) {
						throw new IllegalArgumentException(
								String.format("%s at line %d", e.getMessage(), state.lineNum), e);
					}
				}

				state.lineNum++;
				s = in.readLine();
			}
		} catch (final IOException ioe) {
			throw new IllegalArgumentException(String
					.format("IOError in %s", sourceId));
		}
	}

	public static final int encodeRegister(final String s) {
		final Integer result = REGISTER_CODES.get(s.toLowerCase());

		if (result == null) {
			throw new IllegalArgumentException("Unknown register: ".concat(s));
		}

		return result;
	}

	public static final void main(final String[] args) {
		if (args.length != 1) {
			System.err.println("Usage: command <filepath>");
			System.exit(1);
		}

		try {
			doCompileFromPath(args[0]);
		} catch (final IllegalArgumentException iae) {
			System.err.format("Compile-time error in %s : %s\n", args[0], iae.getMessage());
			System.exit(1);
		} catch (final RuntimeException re) {
			System.err.format(re.getMessage());
			System.exit(1);
		}
	}

	public static final void doCompileFromPath(final String path)
			throws IllegalArgumentException {
		try (final OutputStream fout = new FileOutputStream(path.concat(".out"));
			 final OutputStream flout = new FileOutputStream(path.concat(".labels.txt.out"))) {
			final CompilerData state = new CompilerDataFileOutput("file:".concat(path), fout, flout);

			doCompileFromPathToState(path, state);
			state.output();
		} catch (final FileNotFoundException fnfe) {
			throw new IllegalArgumentException(
					String.format("File not found: %s", path));
		} catch (final IOException ie) {
			throw new IllegalArgumentException(String
					.format("IOError in %s", path));
		}
	}

	public static final void doCompileFromPathToState(final String path, final CompilerData state)
			throws IllegalArgumentException {
		try (final InputStream fin = new FileInputStream(path)) {
			compileToState(path, fin, state);
		} catch (final FileNotFoundException fnfe) {
			throw new IllegalArgumentException(
					String.format("File not found: %s", path));
		} catch (final IOException ie) {
			throw new IllegalArgumentException(String
					.format("IOError in %s", path));
		}
	}


	private CompilerMain() {
		throw new UnsupportedOperationException();
	}
}
