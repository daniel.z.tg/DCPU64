package me.danielzgtg.dcpu64.compiler;

public final class LargeCompilingInstruction implements CompilingInstruction {

	private final long[] targetInstructions;

	public LargeCompilingInstruction(final long[] targetInstructions) {
		this.targetInstructions = targetInstructions;
	}

	@Override
	public long[] compile(final CompilerData state, final long linkOffset) {
		return targetInstructions;
	}
}
