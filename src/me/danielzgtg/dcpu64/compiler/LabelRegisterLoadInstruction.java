package me.danielzgtg.dcpu64.compiler;

public class LabelRegisterLoadInstruction implements CompilingInstruction {

	private final String label;
	private final int lineNum;
	private final int registerMask;
	public static final int SIZE = 2;

	public LabelRegisterLoadInstruction(final String label, final int lineNum, final int register) {
		this.label = label;
		this.lineNum = lineNum;
		this.registerMask = register << 8;
	}

	@Override
	public long[] compile(final CompilerData state, final long linkOffset) {
		final long target = state.resolveLabel(label, lineNum, linkOffset);

		return new long[] { registerMask | 0x0001, target };
	}
}
