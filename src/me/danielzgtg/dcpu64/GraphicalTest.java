package me.danielzgtg.dcpu64;

import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class GraphicalTest {
	public static void main(final String[] ignore) {

		int w = 640;
		int h = 480;
		int type = BufferedImage.TYPE_INT_RGB;

		BufferedImage image = new BufferedImage(w/2, h/2, type);

		int color = 255; // RGBA value, each component in a byte
		Random random = new Random();

		JFrame frame = new JFrame();
		JPanel panel = new JPanel() {
			@Override
			public void paintComponent(final Graphics g) {
				for(int x = 0; x < (w/2); x++) {
					for(int y = 0; y < (h/2); y++) {
						image.setRGB(x, y, (int) (0xFFFFFF * random.nextDouble()));
					}
				}
				g.drawImage(image, 0, 0, w, h, 0, 0, w/2, h/2, null);
			}
		};
		panel.setPreferredSize(new Dimension(w, h));
		frame.add(panel);
		frame.pack();
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setTitle("Test123");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		while (true) {
			frame.repaint();
			try {
				Thread.sleep(50L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
