package me.danielzgtg.dcpu64;

import me.danielzgtg.dcpu64.compiler.CompilerMain;

public class TestingMain {
	public static void main(String[] args) {
		CompilerMain.main(new String[] { "program.dcpu64" });
		Main.main(new String[0]);
	}
}
