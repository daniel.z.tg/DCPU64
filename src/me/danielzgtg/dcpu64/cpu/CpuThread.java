package me.danielzgtg.dcpu64.cpu;

import me.danielzgtg.dcpu64.DCPU64;

public class CpuThread extends Thread {

	public final int threadId;
	public final DCPU64 emulator;

	public int pc;
	public long ax = 0;
	public long dx = 0;
	public int sp = 0;
	public int sbp = 0;
	public long bx = 0;
	public long cx = 0;
	public int si = 0;
	public int di = 0;
	/*
	// ax: vf cf sf zf
	public boolean zf;
	public boolean sf;
	public boolean cf;
	public boolean vf;
	*/

	public volatile CpuState state = CpuState.SUSPENDED;
	public InstructionMode mode = InstructionMode.EXECUTING;
	public int loadTargetReg = 0;

	public static final long CPUID = "danielzgtg".hashCode() | (((long) "official_20170129".hashCode()) << 32);
	private static final boolean DEBUG = false;
	private static final boolean DEBUG_INSTRUCTION_ENABLED = DEBUG;

	public CpuThread(final int threadId, final DCPU64 emulator, final int entryPoint) {
		this.threadId = threadId;
		this.emulator = emulator;
		this.setName(String.format("Cpu Emulation Thread #%d, for %s", threadId, emulator.name));
		this.pc = entryPoint;
	}

	@Override
	public void start() {
		this.state = CpuState.RUNNING;
		super.start();
	}

	@Override
	public void run() {
		if (emulator.threads[this.threadId] != this) {
			throw new IllegalStateException("Wrong emulator for emulation thread");
		}

		long scratch1, scratch2;
		while (this.state != CpuState.STOPPED) {
			//noinspection StatementWithEmptyBody
			while (this.state == CpuState.SUSPENDED) {}

			if ((pc >= emulator.memory.length()) || (pc < 0)) throw new RuntimeException();
			final long fetched = emulator.memory.get(pc);

			if (DEBUG) {
				System.out.format("Cpu %d Stepping: pc @ 0x%08X data = 0x%016X ", threadId, pc, fetched);
			}

			switch (this.mode) {
				case EXECUTING: {
					final int instruction;
					{
						if (fetched > 0xFFFF) {
							throw new RuntimeException(); // TODO proper error handling
						}

						instruction = (int) fetched;
					}

					switch (instruction & 0xFF) {
						case 0x00: { // nop
							if (DEBUG) System.out.println("; NOP");
						}
						break;
						case 0x01: { // reg
							this.loadTargetReg = (instruction >>> 8) & 0xF;
							if (DEBUG) System.out.format("; REG %s\n", nameReg(this.loadTargetReg));

							this.mode = InstructionMode.LOADING;
						}
						break;
						case 0x02: { // mem
							final long target = getReg((instruction >>> 8) & 0xF);
							final long data = getReg((instruction >>> 12) & 0xF);

							if (DEBUG) System.out.format("; MEM %s %s 0x%016X 0x%016X\n",
									nameReg((instruction >>> 8) & 0xF),
									nameReg((instruction >>> 12) & 0xF),
									target, data);
							if ((target >= emulator.memory.length()) || (target < 0)) throw new RuntimeException();

							emulator.memory.set((int) target, data);
						}
						break;
						case 0x03: { // add
							if (DEBUG) System.out.format("; ADD %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));

							final int target = (instruction >>> 8) & 0xF;
							setReg(target, getReg(target) + getReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x04: { // sub
							if (DEBUG) System.out.format("; SUB %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));

							final int target = (instruction >>> 8) & 0xF;
							setReg(target, getReg(target) - getReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x05: { // mul
							if (DEBUG) System.out.format("; MUL %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));

							final int target = (instruction >>> 8) & 0xF;
							setReg(target, getReg(target) * getReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x06: {// out
							if (DEBUG) {
								scratch1 = this.ax;
								scratch2 = this.dx;
								System.out.println();
							}

							if (this.dx > emulator.ports.length) throw new RuntimeException();
							emulator.ports[(int) this.dx].write(this);

							if (DEBUG) {
								System.out.format("; OUT 0x%016X 0x%016X 0x%016X 0x%016X\n",
										scratch1, scratch2, this.ax, this.dx);
							}
						}
						break;
						case 0x07: {// in
							if (DEBUG) {
								scratch1 = this.ax;
								scratch2 = this.dx;
								System.out.println();
							}

							if (this.dx > emulator.ports.length) throw new RuntimeException();
							emulator.ports[(int) this.dx].read(this);

							if (DEBUG) {
								System.out.format("; IN 0x%016X 0x%016X 0x%016X 0x%016X\n",
										scratch1, scratch2, this.ax, this.dx);
							}
						}
						break;
						case 0x08: { // jmp
							final long target = getReg((instruction >>> 8) & 0xF);

							if (DEBUG)
								System.out.format("; JMP %s 0x%016X\n", nameReg((instruction >>> 8) & 0xF), target);
							if ((target >= emulator.memory.length()) || (target < 0)) throw new RuntimeException();

							pc = (int) target;
						}
						continue;
						case 0x09: { // rjmp
							final long target = getReg((instruction >>> 8) & 0xF) + pc;

							if (DEBUG)
								System.out.format("; RJMP %s 0x%016X\n", nameReg((instruction >>> 8) & 0xF), target);
							if ((target >= emulator.memory.length()) || (target < 0)) throw new RuntimeException();

							pc = (int) target;
						}
						continue;
						case 0x0A: // hlt
							if (DEBUG)
								System.out.println("; HLT");

							this.state = CpuState.STOPPED;
							break;
						case 0x0B: { // test
							final long reg1 = getReg((instruction >>> 8) & 0xF);
							final long reg2 = getReg((instruction >>> 12) & 0xF);
							final long result = reg1 & reg2;

							this.ax = (result == 0 ? 1 : 0) | ((result & Long.MIN_VALUE) >>> 62);
							if (DEBUG) System.out.format("; TEST %s %s 0x%016X\n",
									getReg((instruction >>> 8) & 0xF), ((instruction >>> 12) & 0xF), this.ax);
							
							/*
							this.zf = result == 0;
							this.sf = (result & Long.MIN_VALUE) != 0;
							*/
						}
						break;
						case 0x0C: { // cmp
							final long reg1 = getReg((instruction >>> 8) & 0xF);
							final long reg2 = getReg((instruction >>> 12) & 0xF);
							final long result = reg2 - reg1;

							this.ax = (result == 0 ? 0b0001 : 0) | (result < 0 ? 0b0010 : 0) |
									(Long.compareUnsigned(reg2, reg1) > 0 ? 0b0100 : 0) |
									(reg2 > reg1 ? 0b1000 : 0);
							if (DEBUG) System.out.format("; CMP %s %s 0x%016X\n",
									getReg((instruction >>> 8) & 0xF), ((instruction >>> 12) & 0xF), this.ax);
							/*
							this.zf = reg1 == reg2;
							this.vf = reg2 > reg1;
							this.sf = result < 0;
							this.cf = Long.compareUnsigned(reg2, reg1) > 0;
							*/
						}
						break;
						case 0x0D: // cpuid
							if (DEBUG) System.out.format("; CPUID %s\n", CPUID);
							this.ax = CPUID;
							break;
						// skip 3
						case 0x0E: { // sz, se, zf != 0
							// ax: vf cf sf zf
							if ((this.ax & 0b0001) != 0) {
								pc += 3;
								if (DEBUG) System.out.format("; SZ taken\n");
							} else if (DEBUG) System.out.format("; SZ skipped\n");
						}
						break;
						case 0x0F: { // snz, sne, zf == 0
							// ax: vf cf sf zf
							if ((this.ax & 0b0001) == 0) {
								pc += 3;
								if (DEBUG) System.out.format("; SNZ taken\n");
							} else if (DEBUG) System.out.format("; SNZ skipped\n");
						}
						break;
						case 0x10: { // ss, sf != 0
							// ax: vf cf sf zf
							if ((this.ax & 0b0010) != 0) {
								pc += 3;
								if (DEBUG) System.out.format("; SS taken\n");
							} else if (DEBUG) System.out.format("; SS skipped\n");
						}
						break;
						case 0x11: { // sns, sf == 0
							// ax: vf cf sf zf
							if ((this.ax & 0b0010) == 0) {
								pc += 3;
								if (DEBUG) System.out.format("; SNS taken\n");
							} else if (DEBUG) System.out.format("; SNS skipped\n");
						}
						break;
						case 0x12: { // sc, sb, cf != 0
							// ax: vf cf sf zf
							if ((this.ax & 0b0100) != 0) {
								pc += 3;
								if (DEBUG) System.out.format("; SB/SC taken\n");
							} else if (DEBUG) System.out.format("; SB/SC skipped\n");
						}
						break;
						case 0x13: { // snc, sae, cf == 0
							// ax: vf cf sf zf
							if ((this.ax & 0b0100) == 0) {
								pc += 3;
								if (DEBUG) System.out.format("; SAE/SNC taken\n");
							} else if (DEBUG) System.out.format("; SAE/SNC skipped\n");
						}
						break;
						case 0x14: { // sv, vf != 0
							// ax: vf cf sf zf
							if ((this.ax & 0b1000) != 0) {
								pc += 3;
								if (DEBUG) System.out.format("; SV taken\n");
							} else if (DEBUG) System.out.format("; SV skipped\n");
						}
						break;
						case 0x15: { // snv, vf == 0
							// ax: vf cf sf zf
							if ((this.ax & 0b1000) == 0) {
								pc += 3;
								if (DEBUG) System.out.format("; SNV taken\n");
							} else if (DEBUG) System.out.format("; SNV skipped\n");
						}
						break;
						case 0x16: { // sbe, cf == 1 || zf == 1
							// ax: vf cf sf zf
							if ((this.ax & 0b0101) != 0) {
								pc += 3;
								if (DEBUG) System.out.format("; SBE taken\n");
							} else if (DEBUG) System.out.format("; SBE skipped\n");
						}
						break;
						case 0x17: { // sa, cf == 0 && zf == 0
							// ax: vf cf sf zf
							if ((this.ax & 0b0101) == 0) {
								pc += 3;
								if (DEBUG) System.out.format("; SA taken\n");
							} else if (DEBUG) System.out.format("; SA skipped\n");
						}
						break;
						case 0x18: { // sl, sf != vf
							// ax: vf cf sf zf
							if ((this.ax & 0b0010) != ((this.ax & 0b1000) >>> 2)) {
								pc += 3;
								if (DEBUG) System.out.format("; SL taken\n");
							} else if (DEBUG) System.out.format("; SL skipped\n");
						}
						break;
						case 0x19: { // sge, sf == vf
							// ax: vf cf sf zf
							if ((this.ax & 0b0010) == ((this.ax & 0b1000) >>> 2)) {
								pc += 3;
								if (DEBUG) System.out.format("; SGE taken\n");
							} else if (DEBUG) System.out.format("; SGE skipped\n");
						}
						break;
						case 0x1A: { // sle, zf == 1 || sf != vf
							// ax: vf cf sf zf
							if (((this.ax & 0b0001) == 0) || ((this.ax & 0b0010) != ((this.ax & 0b1000) >>> 2))) {
								pc += 3;
								if (DEBUG) System.out.format("; SLE taken\n");
							} else if (DEBUG) System.out.format("; SLE skipped\n");
						}
						break;
						case 0x1B: { // sg, zf  == 0 && sf == vf
							// ax: vf cf sf zf
							if (((this.ax & 0b0001) == 0) && ((this.ax & 0b0010) == ((this.ax & 0b1000) >>> 2))) {
								pc += 3;
								if (DEBUG) System.out.format("; SG taken\n");
							} else if (DEBUG) System.out.format("; SG skipped\n");
						}
						break;
						// leap 3
						case 0x1C: { // le
							if (getReg((instruction >>> 8) & 0xF) == getReg((instruction >>> 12) & 0xF)) {
								pc += 3;
								if (DEBUG) System.out.format("; LE taken %s %s\n",
										nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
							} else if (DEBUG) System.out.format("; LE skipped %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x1D: { // lne
							if (getReg((instruction >>> 8) & 0xF) != getReg((instruction >>> 12) & 0xF)) {
								pc += 3;
								if (DEBUG) System.out.format("; LNE taken %s %s\n",
										nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
							} else if (DEBUG) System.out.format("; LNE skipped %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x1E: { // lz
							if (getReg((instruction >>> 8) & 0xF) == 0) {
								pc += 3;
								if (DEBUG) System.out.format("; LZ taken %s\n",
										nameReg((instruction >>> 8) & 0xF));
							} else if (DEBUG) System.out.format("; LZ skipped %s\n",
									nameReg((instruction >>> 8) & 0xF));
						}
						break;
						case 0x1F: { // lnz
							if (getReg((instruction >>> 8) & 0xF) != 0) {
								pc += 3;
								if (DEBUG) System.out.format("; LNZ taken %s\n",
										nameReg((instruction >>> 8) & 0xF));
							} else if (DEBUG) System.out.format("; LNZ skipped %s\n",
									nameReg((instruction >>> 8) & 0xF));
						}
						break;
						case 0x20: { // ls
							if (getReg((instruction >>> 8) & 0xF) < 0) {
								pc += 3;
								if (DEBUG) System.out.format("; LS taken %s\n",
										nameReg((instruction >>> 8) & 0xF));
							} else if (DEBUG) System.out.format("; LS skipped %s\n",
									nameReg((instruction >>> 8) & 0xF));
						}
						break;
						case 0x21: { // lns
							if (getReg((instruction >>> 8) & 0xF) >= 0) {
								pc += 3;
								if (DEBUG) System.out.format("; LNS taken %s\n",
										nameReg((instruction >>> 8) & 0xF));
							} else if (DEBUG) System.out.format("; LNS skipped %s\n",
									nameReg((instruction >>> 8) & 0xF));
						}
						break;
						case 0x22: { // lb
							if (getReg((instruction >>> 8) & 0xF) < getReg((instruction >>> 12) & 0xF)) {
								pc += 3;
								if (DEBUG) System.out.format("; LB taken %s %s\n",
										nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
							} else if (DEBUG) System.out.format("; LB skipped %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x23: { // lbe
							if (getReg((instruction >>> 8) & 0xF) <= getReg((instruction >>> 12) & 0xF)) {
								pc += 3;
								if (DEBUG) System.out.format("; LBE taken %s %s\n",
										nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
							} else if (DEBUG) System.out.format("; LBE skipped %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x24: { // lg
							if (getReg((instruction >>> 8) & 0xF) > getReg((instruction >>> 12) & 0xF)) {
								pc += 3;
								if (DEBUG) System.out.format("; LG taken %s %s\n",
										nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
							} else if (DEBUG) System.out.format("; LG skipped %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x25: { // lge
							if (getReg((instruction >>> 8) & 0xF) >=
									getReg((instruction >>> 12) & 0xF)) {
								pc += 3;
								if (DEBUG) System.out.format("; LGE taken %s %s\n",
										nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
							} else if (DEBUG) System.out.format("; LGE skipped %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x26: { // not
							if (DEBUG) System.out.format("; NOT %s\n", nameReg((instruction >>> 8) & 0xF));

							final int register = (instruction >>> 8) & 0xF;
							setReg(register, ~getReg(register));
						}
						break;
						case 0x27: { // and
							if (DEBUG) System.out.format("; AND %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));

							final int register1 = (instruction >>> 8) & 0xF;
							setReg(register1, getReg(register1) & getReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x28: { // or
							if (DEBUG) System.out.format("; OR %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));

							final int register1 = (instruction >>> 8) & 0xF;
							setReg(register1, getReg(register1) | getReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x29: { // xor
							if (DEBUG) System.out.format("; XOR %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));

							final int register1 = (instruction >>> 8) & 0xF;
							setReg(register1, getReg(register1) ^ getReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x2A: { // inc
							if (DEBUG) System.out.format("; INC %s\n", nameReg((instruction >>> 8) & 0xF));

							final int register = (instruction >>> 8) & 0xF;
							setReg(register, getReg(register) + 1);
						}
						break;
						case 0x2B: { // dec
							if (DEBUG) System.out.format("; DEC %s\n", nameReg((instruction >>> 8) & 0xF));

							final int register = (instruction >>> 8) & 0xF;
							setReg(register, getReg(register) - 1);
						}
						break;
						case 0x2C: { // shl, rol
							if (DEBUG) System.out.format("; SHL/ROL %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));

							final int register1 = (instruction >>> 8) & 0xF;
							setReg(register1, getReg(register1) << getReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x2D: { // shr
							if (DEBUG) System.out.format("; SHR %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));

							final int register1 = (instruction >>> 8) & 0xF;
							setReg(register1, getReg(register1) >>> getReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x2E: { // ror
							if (DEBUG) System.out.format("; ROR %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));

							final int register1 = (instruction >>> 8) & 0xF;
							setReg(register1, getReg(register1) >> getReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x2F: { // load
							if (DEBUG) System.out.format("; LOAD %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));

							final long target = getReg((instruction >>> 12) & 0xF);
							if ((target >= emulator.memory.length()) || (target < 0)) throw new RuntimeException();

							setReg((instruction >>> 8) & 0xF, emulator.memory.get((int) target));
						}
						break;
						case 0x30: { // push
							final int target = --this.sp;

							if (DEBUG) System.out.format("; PUSH %s 0x%08X\n",
									nameReg((instruction >>> 8) & 0xF), target);
							if ((target >= emulator.memory.length()) || (target < 0)) throw new RuntimeException();

							emulator.memory.set(target, getReg((instruction >>> 8) & 0xF));
						}
						break;
						case 0x31: { // pop
							final int target = this.sp++;

							if (DEBUG) System.out.format("; POP %s 0x%08X\n",
									nameReg((instruction >>> 8) & 0xF), target);
							if ((target >= emulator.memory.length()) || (target < 0)) throw new RuntimeException();

							setReg((instruction >>> 8) & 0xF, emulator.memory.get(target));
						}
						break;
						case 0x32: { // enter, push sbp, + mov sbp, sp
							final int target = --this.sp;

							if (DEBUG) System.out.format("; ENTER 0x%08X\n", target);
							if ((target >= emulator.memory.length()) || (target < 0)) throw new RuntimeException();

							emulator.memory.set(target, this.sbp);
							this.sbp = this.sp;
						}
						break;
						case 0x33: { // leave, mov sp, sbp + pop sbp
							final int target = this.sbp;

							if (DEBUG) System.out.format("; LEAVE 0x%08X\n", target);
							if ((target >= emulator.memory.length()) || (target < 0)) throw new RuntimeException();

							this.sp = target + 1;
							this.sbp = (int) emulator.memory.get(target);
						}
						break;
						case 0x34: { // call, push pc + 1 + jmp target
							final long target = getReg((instruction >>> 8) & 0xF);
							final int newsp = --this.sp;

							if (DEBUG) System.out.format("; CALL %s 0x%08X 0x%08X\n",
									nameReg((instruction >>> 8) & 0xF), target, newsp);
							if ((target >= emulator.memory.length()) || (target < 0)) throw new RuntimeException();
							if ((newsp >= emulator.memory.length()) || (newsp < 0)) throw new RuntimeException();

							emulator.memory.set(newsp, this.pc + 1);
							this.pc = (int) target;
						}
						continue;
						case 0x35: { // ret, pop pc
							final long newsp = this.sp++;
							if ((newsp >= emulator.memory.length()) || (newsp < 0)) throw new RuntimeException();

							final long target = emulator.memory.get((int) newsp);
							if ((target >= emulator.memory.length()) || (target < 0)) throw new RuntimeException();

							if (DEBUG) System.out.format("; RET %s 0x%08X 0x%08X\n",
									nameReg((instruction >>> 8) & 0xF), target, newsp);

							this.pc = (int)target;
						}
						continue;
						case 0x36: { // mov
							if (DEBUG) System.out.format("; MOV %s %s\n",
									nameReg((instruction >>> 8) & 0xF), nameReg((instruction >>> 12) & 0xF));
							setReg((instruction >>> 8) & 0xF, getReg((instruction >>> 12) & 0xF));
						}
						break;
						case 0x37: { // xchg
							final int reg1 = (instruction >>> 8) & 0xF;
							final int reg2 = (instruction >>> 12) & 0xF;

							if (DEBUG) System.out.format("; XCHG %s %s\n",
									nameReg(reg1), nameReg(reg2));
							if ((reg2 >= emulator.memory.length()) || (reg2 < 0)) throw new RuntimeException();

							setReg(reg2, emulator.memory.getAndSet((int) getReg(reg1), getReg(reg2)));
						}
						break;
						case 0x38: { // rcall, push pc + 1 + jmp target + pc
							final long target = getReg((instruction >>> 8) & 0xF) + this.pc;
							final int newsp = --this.sp;

							if (DEBUG) System.out.format("; RCALL %s 0x%08X 0x%08X\n",
									nameReg((instruction >>> 8) & 0xF), target, newsp);
							if ((target >= emulator.memory.length()) || (target < 0)) throw new RuntimeException();
							if ((newsp >= emulator.memory.length()) || (newsp < 0)) throw new RuntimeException();

							emulator.memory.set(newsp, this.pc + 1);
							this.pc = (int) target;
						}
						continue;
						// 0x39 aka debug_breakpoint must be right before default
						case 0x39: // debug_breakpoint
							if (DEBUG_INSTRUCTION_ENABLED) {
								System.out.println("; DEBUG_BREAKPOINT");
								break;
							}
							// slide to default case
						default:
							throw new RuntimeException(String.format("Invalid instruction %d at %d",
									instruction, pc)); // TODO proper error handling
					}

					// Warning: only put pc++ here
					pc++;
				}
				break;
				case LOADING: {
					if (DEBUG) {
						System.out.format("; load 0x%016X or %20d into %s\n", fetched, fetched, nameReg(loadTargetReg));
					}

					setReg(loadTargetReg, fetched);
					pc++;
					this.mode = InstructionMode.EXECUTING;
				}
				break;
			}
		}

		emulator.threads[this.threadId] = null;
		emulator.threads = emulator.threads;

		synchronized (emulator.joinWaitObj) {
			emulator.joinWaitObj.notifyAll();
		}
	}

	public String nameReg(final int register) {
		switch (register) {
			case 0x0:
				return "ax";
			case 0x1:
				return "dx";
			case 0x2:
				return "sp";
			case 0x3:
				return "sbp";
			case 0x4:
				return "pc";
			case 0x5:
				return "bx";
			case 0x6:
				return "cx";
			case 0x7:
				return "si";
			case 0x8:
				return "di";
			default:
				throw new RuntimeException(); // TODO proper error handling
		}
	}

	public void setReg(final int register, final long data) {
		switch (register) {
			case 0x0:
				this.ax = data;
				break;
			case 0x1:
				this.dx = data;
				break;
			case 0x2:
				this.sp = (int) data;
				break;
			case 0x3:
				this.sbp = (int) data;
				break;
			case 0x4:
				if ((data >= emulator.memory.length()) || (data < 0)) throw new RuntimeException();
				this.pc = (int) data - 1;
				break;
			case 0x5:
				this.bx = data;
				break;
			case 0x6:
				this.cx = data;
				break;
			case 0x7:
				this.si = (int) data;
				break;
			case 0x8:
				this.di = (int) data;
				break;
			default:
				throw new RuntimeException(); // TODO proper error handling
		}
	}

	public long getReg(final int register) {
		switch (register) {
			case 0x0:
				return this.ax;
			case 0x1:
				return this.dx;
			case 0x2:
				return this.sp;
			case 0x3:
				return this.sbp;
			case 0x4:
				return this.pc;
			case 0x5:
				return this.bx;
			case 0x6:
				return this.cx;
			case 0x7:
				return this.si;
			case 0x8:
				return this.di;
			default:
				throw new RuntimeException(); // TODO proper error handling
		}
	}

}
