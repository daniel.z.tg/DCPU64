package me.danielzgtg.dcpu64.cpu;

public enum InstructionMode {
	EXECUTING,
	LOADING;
}
