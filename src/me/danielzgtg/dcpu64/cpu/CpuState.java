package me.danielzgtg.dcpu64.cpu;

public enum CpuState {
	RUNNING,
	SUSPENDED,
	STOPPED;
}
