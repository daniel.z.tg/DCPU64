package me.danielzgtg.dcpu64.device;

import me.danielzgtg.dcpu64.DCPU64;
import me.danielzgtg.dcpu64.cpu.CpuThread;

public class PortDeviceConsolePrintNum extends PortDevice {

	public PortDeviceConsolePrintNum(final DCPU64 emulator) {
		super(emulator);
	}

	@Override
	public void write(final CpuThread cpuThread) {
		cpuThread.dx = 0;
		long data = cpuThread.ax;
		System.out.println(String.format("0x%016X or %020d", data, data));
	}

	@Override
	public void read(final CpuThread cpuThread) {
		cpuThread.dx = 1;
	}
}
