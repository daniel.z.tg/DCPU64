package me.danielzgtg.dcpu64.device;

import me.danielzgtg.dcpu64.DCPU64;
import me.danielzgtg.dcpu64.cpu.CpuThread;

public abstract class PortDevice {

	public final DCPU64 emulator;

	protected PortDevice(final DCPU64 emulator) {
		this.emulator = emulator;
	}

	public abstract void write(final CpuThread cpuThread);

	public abstract void read(final CpuThread cpuThread);
}
