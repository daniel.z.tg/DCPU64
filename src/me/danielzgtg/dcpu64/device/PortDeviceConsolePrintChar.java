package me.danielzgtg.dcpu64.device;

import me.danielzgtg.dcpu64.DCPU64;
import me.danielzgtg.dcpu64.cpu.CpuThread;

public class PortDeviceConsolePrintChar extends PortDevice {

	public PortDeviceConsolePrintChar(final DCPU64 emulator) {
		super(emulator);
	}

	@Override
	public void write(final CpuThread cpuThread) {
		long data = cpuThread.ax;

		if ((data < Byte.MAX_VALUE) && (data >= 0)) {
			System.out.print((char) data);
			cpuThread.dx = 0;
		} else {
			cpuThread.dx = 1;
		}
	}

	@Override
	public void read(final CpuThread cpuThread) {
		cpuThread.dx = 1;
	}
}
