package me.danielzgtg.dcpu64.device;

import java.util.concurrent.atomic.AtomicLongArray;

import me.danielzgtg.dcpu64.DCPU64;
import me.danielzgtg.dcpu64.cpu.CpuThread;

public class PortDeviceEchoBuf extends PortDevice {

	public AtomicLongArray ringBuf;
	public final Object lock = new Object();
	public volatile int head = 0;
	public volatile int tail = 0;

	public PortDeviceEchoBuf(final DCPU64 emulator, final int ringBufSize) {
		super(emulator);
		this.ringBuf = new AtomicLongArray(ringBufSize);
	}

	@Override
	public void write(final CpuThread cpuThread) {
		synchronized (lock) {
			final int newPos  = (head + 1) % ringBuf.length();
			if (newPos == tail) {
				cpuThread.dx = 1;
				return;
			}

			ringBuf.set(head, cpuThread.ax);
			head = newPos;

			cpuThread.dx = 0;
		}
	}

	@Override
	public void read(final CpuThread cpuThread) {
		synchronized (lock) {
			if (tail == head) {
				cpuThread.dx = 1;
				return;
			}

			final long read = ringBuf.get(tail);
			tail = (tail + 1) % ringBuf.length();

			cpuThread.ax = read;
			cpuThread.dx = 0;
		}
	}
}
