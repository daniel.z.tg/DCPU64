package me.danielzgtg.dcpu64.device;

import java.util.concurrent.atomic.AtomicLongArray;
import java.util.function.BiFunction;

import me.danielzgtg.dcpu64.DCPU64;
import me.danielzgtg.dcpu64.cpu.CpuThread;

public class PortDeviceGPGPU extends PortDevice {

	private static final int MAX_COMMANDS = 7;

	public PortDeviceGPGPU(final DCPU64 emulator) {
		super(emulator);
	}

	@Override
	public void write(final CpuThread cpuThread) {
		final int upperbound = cpuThread.emulator.memory.length();
		final long inputTarget = cpuThread.ax;

		if ((inputTarget < 0) || ((inputTarget + 10) >= upperbound)) {
			cpuThread.dx = 1;
			return;
		}

		final long switchTarget = cpuThread.emulator.memory.get((int) inputTarget);

		if (switchTarget == 0) {
			cpuThread.dx = 0;
			return;
		}

		if ((switchTarget >= MAX_COMMANDS ) || (switchTarget < 0)) {
			cpuThread.dx = 1;
			return;
		}

		final long target = inputTarget + 1;
		switch ((int) switchTarget) {
			case 1: {
				/*
				 * cmd: fill
				 * ax: ptr to instruction struct
				 * instruction buffer: { fill_data, fill_x, fill_y, fill_addx, fill_addy, framebuffer_start
				 *                       framebuffer_sizex, framebuffer_sizey, border_sizex, border_sizey }
				 */
				applySimpleGPGPU(cpuThread, upperbound, (int) target, (x, y) -> y);
			}
			return;
			case 2: {
				/*
				 * cmd: add
				 * ax: ptr to instruction struct
				 * instruction buffer: { fill_data, fill_x, fill_y, fill_addx, fill_addy, framebuffer_start
				 *                       framebuffer_sizex, framebuffer_sizey, border_sizex, border_sizey }
				 */
				applySimpleGPGPU(cpuThread, upperbound, (int) target, (x, y) -> x + y);
			}
			return;
			case 3: {
				/*
				 * cmd: sub
				 * ax: ptr to instruction struct
				 * instruction buffer: { fill_data, fill_x, fill_y, fill_addx, fill_addy, framebuffer_start
				 *                       framebuffer_sizex, framebuffer_sizey, border_sizex, border_sizey }
				 */
				applySimpleGPGPU(cpuThread, upperbound, (int) target, (x, y) -> x - y);
			}
			return;
			case 4: {
				/*
				 * cmd: mul
				 * ax: ptr to instruction struct
				 * instruction buffer: { fill_data, fill_x, fill_y, fill_addx, fill_addy, framebuffer_start
				 *                       framebuffer_sizex, framebuffer_sizey, border_sizex, border_sizey }
				 */
				applySimpleGPGPU(cpuThread, upperbound, (int) target, (x, y) -> x * y);
			}
			return;
			case 5: {
				/*
				 * cmd: max
				 * ax: ptr to instruction struct
				 * instruction buffer: { fill_data, fill_x, fill_y, fill_addx, fill_addy, framebuffer_start
				 *                       framebuffer_sizex, framebuffer_sizey, border_sizex, border_sizey }
				 */
				applySimpleGPGPU(cpuThread, upperbound, (int) target, Math::max);
			}
			return;
			case 6: {
				/*
				 * cmd: min
				 * ax: ptr to instruction struct
				 * instruction buffer: { fill_data, fill_x, fill_y, fill_addx, fill_addy, framebuffer_start
				 *                       framebuffer_sizex, framebuffer_sizey, border_sizex, border_sizey }
				 */
				applySimpleGPGPU(cpuThread, upperbound, (int) target, Math::min);
			}
			return;
		}
	}

	protected static void applySimpleGPGPU(final CpuThread cpuThread, final int upperbound, final int target,
			final BiFunction<Long, Long, Long> shader) {
		final AtomicLongArray memory = cpuThread.emulator.memory;

		final long fill_data = memory.get(target);
		final long fill_x = memory.get(target + 1);
		final long fill_y = memory.get(target + 2);
		final long fill_addx = memory.get(target + 3);
		final long fill_addy = memory.get(target + 4);
		final long framebuffer_start = memory.get(target + 5);
		final long framebuffer_sizex = memory.get(target + 6);
		final long framebuffer_sizey = memory.get(target + 7);
		final long border_sizex = memory.get(target + 8);
		final long border_sizey = memory.get(target + 9);

		if ((fill_addx < 0) || (fill_addy < 0)) {
			cpuThread.dx = 0;
			return;
		}

		if ((framebuffer_start < 0) || (framebuffer_start >= upperbound)) {
			cpuThread.dx = 1;
			return;
		}

		if ((framebuffer_sizex < 0) || (framebuffer_sizey < 0)) {
			cpuThread.dx = 1;
			return;
		}

		final long framebuffer_end = framebuffer_start + (framebuffer_sizex * framebuffer_sizey);

		if ((framebuffer_end < 0) || (framebuffer_end > upperbound)) {
			cpuThread.dx = 1;
			return;
		}

		final int framebuffer_starti = (int) framebuffer_start;
		final int framebuffer_sizexi = (int) framebuffer_sizex;

		if ((fill_x < 0) || (fill_y < 0) || (fill_addx > framebuffer_sizex) || (fill_addy > framebuffer_sizey)) {
			for (int y = 0; y < framebuffer_sizey; y++) {
				for (int x = 0; x < framebuffer_sizex; x++) {
					final int pos = framebuffer_starti + (framebuffer_sizexi * y) + x;

					memory.set(pos, shader.apply(memory.get(pos), fill_data));
				}
			}
		} else {
			final long fill_endx = fill_x + fill_addx;
			final long fill_endy = fill_y + fill_addy;

			if ((border_sizex < 0) || (border_sizey < 0)) {
				for (int y = 0; y < framebuffer_sizey; y++) {
					if ((y >= fill_y) && (y <= fill_endy)) {
						for (int x = 0; x < framebuffer_sizex; x++) {
							if ((x >= fill_x) && (x <= fill_endx)) {
								final int pos = framebuffer_starti + (framebuffer_sizexi * y) + x;

								memory.set(pos, shader.apply(memory.get(pos), fill_data));
							}
						}
					}
				}
			} else {
				final long brushy1 = fill_y + border_sizey;
				final long brushy2 = fill_endy - border_sizey;
				final long brushx1 = fill_x + border_sizex;
				final long brushx2 = fill_endx - border_sizex;

				for (int y = 0; y < framebuffer_sizey; y++) {
					if ((y >= fill_y) && (y <= fill_endy)) {
						for (int x = 0; x < framebuffer_sizex; x++) {
							if ((x >= fill_x) && (x <= fill_endx) && ((x < brushx1) ||
									(x > brushx2) || (y < brushy1) || (y > brushy2))) {
								final int pos = framebuffer_starti + (framebuffer_sizexi * y) + x;

								memory.set(pos, shader.apply(memory.get(pos), fill_data));
							}
						}
					}
				}
			}
		}

		cpuThread.dx = 0;
	}

	@Override
	public void read(final CpuThread cpuThread) {
		cpuThread.dx = MAX_COMMANDS;
	}
}
