package me.danielzgtg.dcpu64.device;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLongArray;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import me.danielzgtg.dcpu64.DCPU64;
import me.danielzgtg.dcpu64.cpu.CpuThread;

public class PortDeviceVideoCard extends PortDevice {

	private final BufferedImage image;
	private final int hw;
	private final int hh;
	private final JFrame frame;

	private PortDeviceVideoCard(final DCPU64 emulator, final BufferedImage image, final int hw, final int hh,
			final JFrame frame) {
		super(emulator);

		this.image = image;
		this.hw = hw;
		this.hh = hh;
		this.frame = frame;
	}

	public void setTitle(final String title) {
		frame.setTitle(title);
	}

	public static PortDeviceVideoCard init(final DCPU64 emulator, final int w, final int h, final String title,
			final int scaleDown, final Runnable shutdownCallback) {
		if ((w % scaleDown != 0)|| (h % scaleDown != 0)) {
			throw new IllegalArgumentException();
		}

		final int hw = w / scaleDown;
		final int hh = h / scaleDown;

		final BufferedImage image = new BufferedImage(hw, hh, BufferedImage.TYPE_INT_RGB);

		{
			final Random random = new Random();
			for (int x = 0; x < hw; x++) {
				for (int y = 0; y < hh; y++) {
					image.setRGB(x, y, (int) (0xFFFFFF * random.nextDouble()));
				}
			}
		}

		final JFrame frame = new JFrame();
		final JPanel panel = new JPanel() {
			@Override
			public void paintComponent(final Graphics g) {
				g.drawImage(image, 0, 0, w, h, 0, 0, hw, hh, null);
			}
		};
		panel.setPreferredSize(new Dimension(w, h));
		frame.add(panel);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setTitle(title);

		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				shutdownCallback.run();
			}
		});

		return new PortDeviceVideoCard(emulator, image, hw, hh, frame);
	}

	@Override
	public void write(final CpuThread cpuThread) {
		final AtomicLongArray memory = cpuThread.emulator.memory;
		final int upperbound = memory.length();
		final long target = cpuThread.ax;

		if ((target < 0) || ((target + 1) >= upperbound)) {
			cpuThread.dx = 1;
			return;
		}

		final int targeti = (int) target;

		final long framebuffer_sizex = memory.get(targeti);
		final long framebuffer_sizey = memory.get(targeti + 1);

		if ((framebuffer_sizex != this.hw) || (framebuffer_sizey != this.hh)) {
			cpuThread.dx = 1;
			return;
		}

		final int framebuffer_starti = (int) (target + 2);
		final long framebuffer_end = target + framebuffer_sizex * framebuffer_sizey;

		if ((framebuffer_end < 0) || (framebuffer_end >= upperbound)) {
			cpuThread.dx = 1;
			return;
		}

		final int framebuffer_sizexi = (int) framebuffer_sizex;

		for (int y = 0; y < this.hh; y++) {
			for (int x = 0; x < this.hw; x++) {
				int location = framebuffer_starti + (framebuffer_sizexi * y) + x;
				int fetch = (int) memory.get(location);
				this.image.setRGB(x, y, fetch);
			}
		}

		this.frame.repaint();
		cpuThread.dx = 0;
	}

	@Override
	public void read(final CpuThread cpuThread) {
		cpuThread.ax = this.hw;
		cpuThread.dx = this.hh;
	}
}
