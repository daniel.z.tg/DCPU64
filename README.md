
## DCPU64

a.k.a the Demonstration Central Processing Unit-64 (emulator)

Instruction set is very minimal.
Ancient x86-like programs should work.
There are no interrupts, paging, or rings.
Very likely to be turing-complete (todo: verify)

### Examples

See the .dcpu64 files in the project directory.
todo: write an actual example.

## Contributors

daniel.z.tg

## License

All files are LGPL3 unless stated otherwise.
5x7font.* is CC0.
.gitignore is CC0.
*.md is CC0 unless stated otherwise.
LICENSE.md is under an unknown license.
 
